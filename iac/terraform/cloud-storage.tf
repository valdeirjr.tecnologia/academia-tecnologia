resource "google_storage_bucket" "bk-terraform" {
  name          = "bucket-dedup"
  force_destroy = true
  location      = "US"
  uniform_bucket_level_access = true
  storage_class = "STANDARD"
  versioning {
    enabled = true
  }
}