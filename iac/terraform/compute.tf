resource "google_compute_instance" "gce" {
  project      = var.project_id
  name         = "${var.name_prefix}-${var.environment}"
  tags         = [var.environment]
  machine_type = var.machine_type
  zone         = var.zone
  labels = {
    "system" = var.system_label
    "env"    = var.environment
  }

  metadata_startup_script = "echo \"sshd:   10.86.7.        #Redes Gitlab runners GCP\" >> /etc/hosts.allow"
  
  boot_disk {
    initialize_params {
      image = var.source_image
      size  = var.disk_size
    }
  }

  network_interface {
    subnetwork = var.subnetwork
  }

  service_account {
    email  = var.service_account
    scopes = ["cloud-platform"]
  }
}