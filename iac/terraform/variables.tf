variable "project_id" {
  type        = string
  description = "Variável responsável pelo ID do Projeto da GCP."
}

variable "environment" {
  type        = string
  description = "Ambiente que será executado o Terraform"
}

variable "zone" {
  type        = string
  default     = "us-east1-b"
  description = "Variável da zona de Disponibilidade K6 Qa."
}

variable "machine_type" {
  type        = string
  description = "Variável responsável pelo tipo da instância."
}

variable "source_image" {
  type        = string
  description = "Imagem para ser utilizacada na criação da instância."
}

variable "network" {
  type        = string
  description = "Valor da rede a ser configurada na instância."
}

variable "subnetwork" {
  type        = string
  description = "Valor a subrede a ser configurada na instância."
}

variable "disk_size" {
  type        = number
  default     = 60
  description = "Valor para o tamanho do disco."
}

variable "service_account" {
  type        = string
  default     = "sa-full-free@validacao-dev.iam.gserviceaccount.com"
  description = "Valor da conta padrão de serviço utilizada."
}

variable "name_prefix" {
  type        = string
  description = "Prefixo do nome padrão Globo com padrão para template."
  default     = "us-e1-template-dedup"
}

variable "system_label" {
  type        = string
  description = "Objetivo da maquina virtual"
  default     = "dedup-template"
}

# variable "replicas" {
#   type        = list(string)
#   description = "Replicas Secrets."
#   default     = ["us-east1"]
# }

# variable "secret" {
#   type        = string
#   description = "Valor do secret a ser armazenado no Google."
#   sensitive   = true
# }

# variable "secret_id" {
#   type        = string
#   description = "Identificador do secret."
# }

# variable "secret_manager_viewers" {
#   type        = list(string)
#   description = "Lista dos usuário que podem somente visualizar o secrets."
#   default     = ["serviceAccount:sa-dedup-hml-prod@gglobo-gig-upd-ass-hml-hdg-dev.iam.gserviceaccount.com"]
# }